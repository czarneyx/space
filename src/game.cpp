#include "game.hpp"

Game::Game() {

    sf::Texture tex;
    tex.loadFromFile("res\\gfx\\Pink_Monster\\Pink_Monster.png");
    player.SetTexture(tex);

}

void Game::Start() {

    Log::Debug("Game has started");

}

void Game::Loop() {

    if(sf::Keyboard::isKeyPressed(sf::Keyboard::W)) player.Move(0 * this->dt,-1 * this->dt);
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::A)) { 
        player.Move(-1 * this->dt,0 * this->dt);
    }
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::S)) player.Move(0 * this->dt,1 * this->dt);
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
        player.Move(1 * this->dt,0 * this->dt);
    }
}

void Game::CompareEvents() {

    if(this->event.type == sf::Event::MouseButtonPressed) {

        if(this->event.key.code == sf::Mouse::Left) {

            Log::Debug("Left mouse button is pressed");

        }

    }

}

void Game::Draw() {

    this->player.Draw(this->window);

}