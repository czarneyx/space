#pragma once

#include "engine/engine.hpp"

class Game : public Engine {

public:
    Game();
private:
    void Start();
    void Loop();
    void Draw();
    void CompareEvents();

    Player player;

};