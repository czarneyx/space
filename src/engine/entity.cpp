#include "entity.hpp"

Entity::Entity() {

    

}

void Entity::Draw(sf::RenderWindow *_window) {

    _window->draw(this->sprite);

}

void Entity::Move(float _x, float _y) {

    this->sprite.move(_x, _y);

}

void Entity::Move(sf::Vector2f _deltaPos) {

    this->sprite.move(_deltaPos);

}

void Entity::SetPosition(sf::Vector2f _pos) {

    this->sprite.setPosition(_pos);

}

void Entity::SetPosition(float _x, float _y) {

    this->sprite.setPosition(_x, _y);

}

void Entity::SetTexture(sf::Texture _texture) {

    this->texture = new sf::Texture(_texture);
    this->sprite.setTexture(*this->texture);

    this->sprite.setScale(3, 3);

}