#pragma once

#include "entity.hpp"

enum Animation {

    Idle = 0,
    Walk,
    Run,
    Attack,
    Death,
    Hurt,
    Jump

};

class Player : public Entity {

public:
    Player();

    void Move(float, float);

private:
    float movementSpeed;
    
};