#include "engine.hpp"

Engine::Engine() {

    this->Init();

}

void Engine::Init() {

    this->InitWindow();

}

void Engine::InitWindow() {

    std::ifstream ifs("config/window_config.ini");

    sf::VideoMode videoMode;

    if(ifs.is_open()) {

        ifs >> videoMode.width >> videoMode.height;

    }

    this->window = new sf::RenderWindow(videoMode, "Game", sf::Style::Resize | sf::Style::Close);

}

void Engine::Run() {

    Start();

    while(this->window->isOpen()) {

        this->Update();
        Loop();
        this->Render();

    }

}

void Engine::Update() {

    this->UpdateEvents();
    this->UpdateDeltaTime();

}

void Engine::UpdateEvents() {

    while(this->window->pollEvent(this->event)) {

        if(this->event.type == sf::Event::Closed) this->Quit();

        CompareEvents();

    }

}

void Engine::UpdateDeltaTime() {

    this->dt = this->dtClock.restart().asSeconds();

}

void Engine::Render() {

    this->window->clear();
    Draw();
    this->window->display();

}

void Engine::Quit() {

    this->window->close();

}

void Engine::Start() {
    


}

void Engine::Loop() {



}

void Engine::Draw() {



}

void Engine::CompareEvents() {



}