#pragma once

#include "player.hpp"

class Engine {

public:
    Engine();

    virtual void Start() = 0;
    void Update();
    void UpdateEvents();
    virtual void CompareEvents() = 0;
    void UpdateDeltaTime();
    virtual void Loop() = 0;
    void Render();
    virtual void Draw() = 0;
    void Run();
    void Quit();

private:
    void Init();
    void InitWindow();

    sf::Clock dtClock;

protected:
    sf::RenderWindow *window;
    sf::Event event;

    sf::Clock clock;
    float dt;

};