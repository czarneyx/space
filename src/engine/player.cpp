#include "player.hpp"

Player::Player() {

    this->movementSpeed = 200;

}

void Player::Move(float _x, float _y) {

    this->sprite.move(_x * this->movementSpeed, _y * this->movementSpeed);

}