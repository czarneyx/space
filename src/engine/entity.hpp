#pragma once

#include <SFML/Graphics.hpp>
#include <fstream>
#include <vector>
#include "log.hpp"

class Entity {

public:
    Entity();

    void SetTexture(sf::Texture);

    void Draw(sf::RenderWindow *);
    void Move(float, float);
    void Move(sf::Vector2f);
    void SetPosition(float, float);
    void SetPosition(sf::Vector2f);

protected:
    sf::Sprite sprite;
    sf::Texture *texture;

};