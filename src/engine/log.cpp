#include "log.hpp"

void Log::Message(std::string _msg) {

    std::cout << "" << _msg << std::endl;

}

void Log::Warning(std::string _msg) {

    std::cout << "[WARNING] - " << _msg << std::endl;

}

void Log::Error(std::string _msg) {

    std::cout << "[ERROR] - " << _msg << std::endl;

}

void Log::Debug(std::string _msg) {

    std::cout << "[DEBUG] - " << _msg << std::endl;

}