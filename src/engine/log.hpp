#pragma once

#include <iostream>
#include <string>

class Log {

public:
    static void Message(std::string);
    static void Warning(std::string);
    static void Error(std::string);
    static void Debug(std::string);

};