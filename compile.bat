g++ -c -w src/engine/engine.cpp -o bin/debug/engine.o -IC:/mingw32/include
g++ -c -w src/engine/entity.cpp -o bin/debug/entity.o -IC:/mingw32/include
g++ -c -w src/engine/log.cpp -o bin/debug/log.o -IC:/mingw32/include
g++ -c -w src/game.cpp -o bin/debug/game.o -IC:/mingw32/include
g++ -c -w src/main.cpp -o bin/debug/main.o -IC:/mingw32/include
g++ -c -w src/engine/player.cpp -o bin/debug/player.o -IC:/mingw32/include

g++ -g -w bin/debug/*.o -o bin/release/prj.exe -LC:/mingw32/lib -lsfml-system -lsfml-window -lsfml-graphics -lsfml-audio -lsfml-network

.\bin\release\prj.exe